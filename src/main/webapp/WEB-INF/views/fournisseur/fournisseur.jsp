<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="fr">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>SB Admin 2 - Bootstrap Admin Theme</title>

<!-- Bootstrap Core CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link
	href="<%=request.getContextPath()%>/resources/dist/css/sb-admin-2.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<%@ include file="/WEB-INF/views/top_menu/topMenu.jsp"%>
			<%@ include file="/WEB-INF/views/left_menu/leftMenu.jsp"%>
			<!-- /.navbar-static-side -->
		</nav>

		<!-- Page Content -->

		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">
							<fmt:message key="common.fournisseur"></fmt:message>
						</h1>
					</div>
					<div class="panel-body">
						<div id="dataTables-example_wrapper"
							class="dataTables_wrapper form-inline dt-bootstrap no-footer">

						</div>
						<!-- /.col-lg-12 -->

						<div class="row">
							<div class="col-lg-12">
								<ol class="breadcrumb">
									<c:url value="/fournisseur/nouveau" var="fournisseur" />
									<li><a href="${fournisseur}"><i class="fa fa-plus">&nbsp;<fmt:message
													key="common.ajouter"></fmt:message></i></a></li>

									<li><a href="#"><i class="fa fa-upload">&nbsp;<fmt:message
													key="common.importer"></fmt:message></i></a></li>
									<li><a href="#"><i class="fa fa-download">&nbsp;<fmt:message
													key="common.exporter"></fmt:message></i></a></li>

									<li class="active">Data</li>
								</ol>
							</div>

						</div>

						<div class="row">
							<div class="panel panel-default">
								<div class="panel-heading">
									<fmt:message key="common.list.fournisseur"></fmt:message>
								</div>
								<!-- /.panel-heading -->
								<div class="panel-body">
									<div id="dataTables-example_wrapper"
										class="dataTables_wrapper form-inline dt-bootstrap no-footer">

									</div>

									<div class="row">
										<div class="col-sm-12">
											<table width="100%"
												class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline"
												id="dataTables-example" role="grid"
												aria-describedby="dataTables-example_info"
												style="width: 100%;">
												<thead>
													<tr role="row">
														<th class="sorting_asc" tabindex="0"
															aria-controls="dataTables-example" rowspan="1"
															colspan="1" aria-sort="ascending"
															aria-label="Rendering engine: activate to sort column descending"
															style="width: 100px;"><i><fmt:message
																	key="common.photo"></fmt:message></i></th>
														<th class="sorting" tabindex="0"
															aria-controls="dataTables-example" rowspan="1"
															colspan="1"
															aria-label="Browser: activate to sort column ascending"
															style="width: 124px;"><i><fmt:message
																	key="common.nom"></fmt:message></i></th>
														<th class="sorting" tabindex="0"
															aria-controls="dataTables-example" rowspan="1"
															colspan="1"
															aria-label="Platform(s): activate to sort column ascending"
															style="width: 113px;"><i><fmt:message
																	key="common.prenom"></fmt:message></i></th>
														<th class="sorting" tabindex="0"
															aria-controls="dataTables-example" rowspan="1"
															colspan="1"
															aria-label="Engine version: activate to sort column ascending"
															style="width: 85px;"><i><fmt:message
																	key="common.adresse"></fmt:message></i></th>
														<th class="sorting" tabindex="0"
															aria-controls="dataTables-example" rowspan="1"
															colspan="1"
															aria-label="CSS grade: activate to sort column ascending"
															style="width: 61px;"><i><fmt:message
																	key="common.mail"></fmt:message></i></th>

														<th class="sorting" tabindex="0"
															aria-controls="dataTables-example" rowspan="1"
															colspan="1"
															aria-label="CSS grade: activate to sort column ascending"
															style="width: 61px;"><i><fmt:message
																	key="common.action"></fmt:message></i></th>

													</tr>
												</thead>
												<tbody>
													<c:forEach items="${fournisseurs}" var="fournisseur">

														<tr class="odd gradeX">
															<td><img src="${fournisseur.getPhoto()}"
																width="50px" height="50px"></td>
															<td>${fournisseur.getNom()}</td>
															<td>${fournisseur.getPrenom()}</td>

															<td>${fournisseur.getAdresse()}</td>
															<td>${fournisseur.getEmail()}</td>


															<td><c:url
																	value="/fournisseur/modifier/${fournisseur.getIdFournisseur()}"
																	var="urlModif" /> <a href="${urlModif}"><i
																	class="fa fa-edit">&nbsp;</i></a> <a href="#"
																data-toggle="modal"
																data-target="#modalClient${fournisseur.getIdFournisseur()}"><i
																	class="fa fa-trash-o">&nbsp; </i></a> <!-- Modal -->

																<div class="modal fade"
																	id="modalClient${fournisseur.getIdFournisseur()}"
																	tabindex="-1" role="dialog"
																	aria-labelledby="myModalLabel" aria-hidden="true">

																	<div class="modal-dialog">
																		<div class="modal-content">
																			<div class="modal-header">
																				<button type="button" class="close"
																					data-dismiss="modal" aria-hidden="true">&times;</button>
																				<h4 class="modal-title" id="myModalLabel">
																					<fmt:message key="common.modal.titre"></fmt:message>
																				</h4>
																			</div>
																			Confirmation
																		</div>
																		<div class="modal-footer">
																			<button type="button" class="btn btn-default"
																				data-dismiss="modal">
																				<fmt:message key="common.annuler"></fmt:message>
																			</button>
																			<c:url
																				value="/fournisseur/supprimer/${fournisseur.getIdFournisseur()}"
																				var="urlSupp" />
																			<a href="${urlSupp}" class="btn btn-danger"><i
																				class="fa fa-trash-o"></i>&nbsp;<fmt:message
																					key="common.confirmer"></fmt:message></a>
																		</div>
																	</div>
																	<!-- /.modal-content -->
																</div> <!-- /.modal-dialog --> <!-- /.modal --></td>
														</tr>
													</c:forEach>


												</tbody>
											</table>
										</div>
									</div>
								</div>

								<div class="well">
									<h4>DataTables Usage Information</h4>
									<p>
										DataTables is a very flexible, advanced tables plugin for
										jQuery. In SB Admin, we are using a specialized version of
										DataTables built for Bootstrap 3. We have also customized the
										table headings to use Font Awesome icons in place of images.
										For complete documentation on DataTables, visit their website
										at <a target="_blank" href="https://datatables.net/">https://datatables.net/</a>.
									</p>
									<a class="btn btn-default btn-lg btn-block" target="_blank"
										href="https://datatables.net/">View DataTables
										Documentation</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/dist/js/sb-admin-2.js"></script>

	<!-- DataTables JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/datatables/js/jquery.dataTables.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendor/datatables-responsive/dataTables.responsive.js"></script>

	<!-- Custom Theme JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/dist/js/sb-admin-2.js"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
		$(document).ready(function() {
			$('#dataTables-example').DataTable({
				responsive : true
			});
		});
	</script>
</body>

</html>
