package com.nice.stock.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.nice.stock.entities.Client;
import com.nice.stock.services.IClientService;
import com.nice.stock.services.IFlickrService;

@Controller
@RequestMapping(value = "/client")
public class ClientController {

	// Déclaration des services
	@Autowired
	private IClientService clientService;

	@Autowired
	private IFlickrService flickservice;

	@RequestMapping(value = "/")
	public String client(Model model) {

		List<Client> clients = clientService.selectAll();

		if (clients == null) {
			clients = new ArrayList<Client>();
		}
		model.addAttribute("clients", clients);
		return "client/client";
	}

	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterClient(Model model) {
		// On passe un objet client vide!
		Client client = new Client();
		// client.setNom("baki");

		model.addAttribute("client", client);

		return "client/ajouterClient";
	}

	@RequestMapping(value = "/nouveau", method = RequestMethod.POST)
	public String enregistrement(Model model, Client client, MultipartFile file) throws Exception {
		String photoUrl = null;
		if (client != null) {

			if (file != null && !file.isEmpty()) {
				InputStream stream = null;

				try {
					stream = file.getInputStream();
					photoUrl = flickservice.savePhoto(stream, client.getNom());
					client.setPhoto(photoUrl);

				} catch (Exception e) {
					e.printStackTrace();
				} finally {

					try {
						stream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}

			}

			if (client.getIdClient() != null) {
				clientService.update(client);
			} else {
				clientService.save(client);
			}

		}

		return "redirect:/client/";
	}

	@RequestMapping(value = "/modifier/{idClient}")
	public String modifierClient(Model model, @PathVariable Long idClient) {
		if (idClient != null) {
			Client client = clientService.getByIg(idClient);
			model.addAttribute("client", client);
		}

		return "client/ajouterClient";
	}

	@RequestMapping(value = "/supprimer/{idClient}")
	public String supprimerlient(Model model, @PathVariable Long idClient) {
		// Mesure sécuritaire!
		if (idClient != null) {
			Client client = clientService.getByIg(idClient);
			if (client != null) {
				clientService.remove(idClient);
			}
		}

		return "redirect:/client/";
	}

}
