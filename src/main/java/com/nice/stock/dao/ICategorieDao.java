package com.nice.stock.dao;

import com.nice.stock.entities.Categorie;

public interface ICategorieDao extends IGenericDao<Categorie> {

}
