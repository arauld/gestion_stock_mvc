package com.nice.stock.dao;

import com.nice.stock.entities.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur> {

}
