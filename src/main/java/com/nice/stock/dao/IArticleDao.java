package com.nice.stock.dao;

import com.nice.stock.entities.Article;

public interface IArticleDao extends IGenericDao<Article> {

}
