package com.nice.stock.dao;

import com.nice.stock.entities.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient> {

}
