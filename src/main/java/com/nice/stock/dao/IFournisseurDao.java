package com.nice.stock.dao;

import com.nice.stock.entities.Fournisseur;

public interface IFournisseurDao extends IGenericDao<Fournisseur> {

}
