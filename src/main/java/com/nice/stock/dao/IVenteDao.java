package com.nice.stock.dao;

import com.nice.stock.entities.Vente;

public interface IVenteDao extends IGenericDao<Vente> {

}
