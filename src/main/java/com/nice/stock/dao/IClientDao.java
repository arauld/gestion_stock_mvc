package com.nice.stock.dao;

import com.nice.stock.entities.Client;

public interface IClientDao extends IGenericDao<Client> {

}
