package com.nice.stock.dao;

import com.nice.stock.entities.CommandeFournisseur;

public interface ICommandeFournisseurDao extends IGenericDao<CommandeFournisseur> {

}
