package com.nice.stock.dao;

import com.nice.stock.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur>{

}
