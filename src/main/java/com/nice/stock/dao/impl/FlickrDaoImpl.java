package com.nice.stock.dao.impl;

import java.io.InputStream;
import java.net.ConnectException;

import javax.swing.JOptionPane;

import org.scribe.model.Token;
import org.scribe.model.Verifier;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.nice.stock.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao {

	private Flickr flickr;
	private UploadMetaData uploadMetaData = new UploadMetaData();

	private String apiKey = "4e66c298eff226800c2d52bb9eafaeed";
	private String sharedSecret = "105197ed6738dfa6";

	private void connect() {

		flickr = new Flickr(apiKey, sharedSecret, new REST());
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157683249412632-f95a871335716d14");
		auth.setTokenSecret("ed26cced2e57d918");

		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}

	@Override
	public String savePhoto(InputStream stream, String fileName) throws Exception {
		connect();
		uploadMetaData.setTitle(fileName);
		String photoId = flickr.getUploader().upload(stream, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}

	public void auth() {
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		AuthInterface authInterface = flickr.getAuthInterface();

		Token token = authInterface.getRequestToken();
		System.out.println("token: " + token);

		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Follow this url to authorize yourself on Flickr");
		System.out.println(url);
		System.out.println("Paste in the token it gives you");

		String tokenKey = JOptionPane.showInputDialog(null);

		Token requestToken = authInterface.getAccessToken(token, new Verifier(tokenKey));

		System.out.println("Authentication success");

		Auth auth = null;

		try {

			auth = authInterface.checkToken(requestToken);

		} catch (FlickrException e) {
			e.printStackTrace();
		}

		System.out.println("Token:" + requestToken.getToken());
		System.out.println("Secret:" + requestToken.getSecret());
		System.out.println("nsid:" + auth.getUser().getId());
		System.out.println("Realname:" + auth.getUser().getRealName());
		System.out.println("Username:" + auth.getUser().getUsername());
		System.out.println("Permission:" + auth.getPermission().getType());
	}
}
