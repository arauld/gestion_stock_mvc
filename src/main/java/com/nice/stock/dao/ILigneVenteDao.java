package com.nice.stock.dao;

import com.nice.stock.entities.LigneVente;

public interface ILigneVenteDao extends IGenericDao<LigneVente> {

}
