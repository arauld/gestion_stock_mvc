package com.nice.stock.dao;

import com.nice.stock.entities.MvtStock;

public interface IMvtStockDao extends IGenericDao<MvtStock> {

}
