package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.ICategorieDao;
import com.nice.stock.entities.Categorie;
import com.nice.stock.services.ICategorieService;

@Transactional
public class CategorieServiceImpl implements ICategorieService {

	private ICategorieDao categorieDao;
	
	public void setCategorieDao(ICategorieDao categorieDao) {
		this.categorieDao = categorieDao;
	}

	@Override
	public Categorie save(Categorie entity) {
		
		return categorieDao.save(entity);
	}

	@Override
	public Categorie update(Categorie entity) {
		
		return categorieDao.update(entity);
	}

	@Override
	public List<Categorie> selectAll() {
		
		return categorieDao.selectAll();
	}

	@Override
	public List<Categorie> selectAll(String sortField, String sort) {
		
		return categorieDao.selectAll();
	}

	@Override
	public Categorie getByIg(Long id) {
		
		return categorieDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
		categorieDao.remove(id);
		
	}

	@Override
	public Categorie findOne(String paramName, Object paramValue) {
		
		return categorieDao.findOne(paramName, paramValue);
	}

	@Override
	public Categorie findOne(String[] paramNames, Object[] paramValues) {
		
		return categorieDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return categorieDao.findCountBy(paramName, paramValue);
	}



}
