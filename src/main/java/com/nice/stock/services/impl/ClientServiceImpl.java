package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.IClientDao;
import com.nice.stock.entities.Client;
import com.nice.stock.services.IClientService;

@Transactional
public class ClientServiceImpl implements IClientService {

	private IClientDao clientDao;
	
	public void setClientDao(IClientDao clientDao) {
		this.clientDao = clientDao;
	}

	@Override
	public Client save(Client entity) {
		// TODO Auto-generated method stub
		return clientDao.save(entity);
	}

	@Override
	public Client update(Client entity) {
		// TODO Auto-generated method stub
		return clientDao.update(entity);
	}

	@Override
	public List<Client> selectAll() {
		// TODO Auto-generated method stub
		return clientDao.selectAll();
	}

	@Override
	public List<Client> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return clientDao.selectAll(sortField, sort);
	}

	@Override
	public Client getByIg(Long id) {
		// TODO Auto-generated method stub
		return clientDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
		clientDao.remove(id);
		
	}

	@Override
	public Client findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return clientDao.findOne(paramName, paramValue);
	}

	@Override
	public Client findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return clientDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return clientDao.findCountBy(paramName, paramValue);
	}

}
