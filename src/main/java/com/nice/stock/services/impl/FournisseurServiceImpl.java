package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.IFournisseurDao;
import com.nice.stock.entities.Fournisseur;
import com.nice.stock.services.IFournisseurService;

@Transactional
public class FournisseurServiceImpl implements IFournisseurService {

	private IFournisseurDao fournisseurDao;
	
	public void setFournisseurDao(IFournisseurDao fournisseurDao) {
		this.fournisseurDao = fournisseurDao;
	}

	@Override
	public Fournisseur save(Fournisseur entity) {
		// TODO Auto-generated method stub
		return fournisseurDao.save(entity);
	}

	@Override
	public Fournisseur update(Fournisseur entity) {
		// TODO Auto-generated method stub
		return fournisseurDao.update(entity);
	}

	@Override
	public List<Fournisseur> selectAll() {
		// TODO Auto-generated method stub
		return fournisseurDao.selectAll();
	}

	@Override
	public List<Fournisseur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return fournisseurDao.selectAll(sortField, sort);
	}

	@Override
	public Fournisseur getByIg(Long id) {
		// TODO Auto-generated method stub
		return fournisseurDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
		fournisseurDao.remove(id);
		
	}

	@Override
	public Fournisseur findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return fournisseurDao.findOne(paramName, paramValue);
	}

	@Override
	public Fournisseur findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return fournisseurDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return fournisseurDao.findCountBy(paramName, paramValue);
	}

}
