package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.ILigneVenteDao;
import com.nice.stock.entities.LigneVente;
import com.nice.stock.services.ILigneVenteService;

@Transactional
public class LigneVenteServiceImpl implements ILigneVenteService {

	private ILigneVenteDao ligneVenteDao;
	 
	public void setLigneVenteDao(ILigneVenteDao ligneVenteDao) {
		this.ligneVenteDao = ligneVenteDao;
	}

	@Override
	public LigneVente save(LigneVente entity) {
		// TODO Auto-generated method stub
		return ligneVenteDao.save(entity);
	}

	@Override
	public LigneVente update(LigneVente entity) {
		// TODO Auto-generated method stub
		return ligneVenteDao.update(entity);
	}

	@Override
	public List<LigneVente> selectAll() {
		// TODO Auto-generated method stub
		return ligneVenteDao.selectAll();
	}

	@Override
	public List<LigneVente> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return ligneVenteDao.selectAll(sortField, sort);
	}

	@Override
	public LigneVente getByIg(Long id) {
		// TODO Auto-generated method stub
		return ligneVenteDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
		ligneVenteDao.remove(id);
		
	}

	@Override
	public LigneVente findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return ligneVenteDao.findOne(paramName, paramValue);
	}

	@Override
	public LigneVente findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return ligneVenteDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return ligneVenteDao.findCountBy(paramName, paramValue);
	}

}
