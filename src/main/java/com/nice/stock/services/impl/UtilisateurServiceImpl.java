package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.IUtilisateurDao;
import com.nice.stock.entities.Utilisateur;
import com.nice.stock.services.IUtilisateurService;

@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService {

	private IUtilisateurDao utilisateurDao;
	 
	public void setUtilisateurDao(IUtilisateurDao utilisateurDao) {
		this.utilisateurDao = utilisateurDao;
	}
	

	@Override
	public Utilisateur save(Utilisateur entity) {
		// TODO Auto-generated method stub
		return  utilisateurDao.save(entity);
	}

	@Override
	public Utilisateur update(Utilisateur entity) {
		// TODO Auto-generated method stub
		return  utilisateurDao.update(entity);
	}

	@Override
	public List<Utilisateur> selectAll() {
		// TODO Auto-generated method stub
		return  utilisateurDao.selectAll();
	}

	@Override
	public List<Utilisateur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return  utilisateurDao.selectAll(sortField, sort);
	}

	@Override
	public Utilisateur getByIg(Long id) {
		// TODO Auto-generated method stub
		return  utilisateurDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		 utilisateurDao.remove(id);
		
	}

	@Override
	public Utilisateur findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return  utilisateurDao.findOne(paramName, paramValue);
	}

	@Override
	public Utilisateur findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return  utilisateurDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return  utilisateurDao.findCountBy(paramName, paramValue);
	}

}
