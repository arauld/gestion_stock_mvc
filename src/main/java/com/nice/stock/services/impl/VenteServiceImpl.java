package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.IVenteDao;
import com.nice.stock.entities.Vente;
import com.nice.stock.services.IVenteService;

@Transactional
public class VenteServiceImpl implements IVenteService {

	 private IVenteDao venteDao;
	 
	public void setVenteDao(IVenteDao venteDao) {
		this.venteDao = venteDao;
	}

	@Override
	public Vente save(Vente entity) {
		// TODO Auto-generated method stub
		return venteDao.save(entity);
	}

	@Override
	public Vente update(Vente entity) {
		// TODO Auto-generated method stub
		return venteDao.update(entity);
	}

	@Override
	public List<Vente> selectAll() {
		// TODO Auto-generated method stub
		return venteDao.selectAll();
	}

	@Override
	public List<Vente> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return venteDao.selectAll(sortField, sort);
	}

	@Override
	public Vente getByIg(Long id) {
		// TODO Auto-generated method stub
		return venteDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		venteDao.remove(id);
	}

	@Override
	public Vente findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return venteDao.findOne(paramName, paramValue);
	}

	@Override
	public Vente findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return venteDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return venteDao.findCountBy(paramName, paramValue);
	}

}
