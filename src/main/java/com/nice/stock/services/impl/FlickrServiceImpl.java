package com.nice.stock.services.impl;

import java.io.InputStream;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.IFlickrDao;
import com.nice.stock.services.IFlickrService;

@Transactional
public class FlickrServiceImpl implements IFlickrService {

	private IFlickrDao flickrDao;
	
	public void setFlickrDao(IFlickrDao flickrDao) {
		this.flickrDao = flickrDao;
	}

	@Override
	public String savePhoto(InputStream stream, String fileName) throws Exception {
		
		return flickrDao.savePhoto(stream, fileName);
	}

}
