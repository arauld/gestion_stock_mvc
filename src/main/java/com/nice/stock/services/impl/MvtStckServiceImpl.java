package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.IMvtStockDao;
import com.nice.stock.entities.MvtStock;
import com.nice.stock.services.IMvtStckService;

@Transactional
public class MvtStckServiceImpl implements IMvtStckService {

	private IMvtStockDao mvtStckDao;
	
	public void setMvtStckDao(IMvtStockDao mvtStckDao) {
		this.mvtStckDao = mvtStckDao;
	}
	

	@Override
	public MvtStock save(MvtStock entity) {
		// TODO Auto-generated method stub
		return mvtStckDao.save(entity);
	}

	@Override
	public MvtStock update(MvtStock entity) {
		// TODO Auto-generated method stub
		return mvtStckDao.update(entity);
	}

	@Override
	public List<MvtStock> selectAll() {
		// TODO Auto-generated method stub
		return mvtStckDao.selectAll();
	}

	@Override
	public List<MvtStock> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return mvtStckDao.selectAll(sortField, sort);
	}

	@Override
	public MvtStock getByIg(Long id) {
		// TODO Auto-generated method stub
		return mvtStckDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		mvtStckDao.remove(id);
	}

	@Override
	public MvtStock findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return mvtStckDao.findOne(paramName, paramValue);
	}

	@Override
	public MvtStock findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return mvtStckDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return mvtStckDao.findCountBy(paramName, paramValue);
	}

}
