package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.ILigneCommandeClientDao;
import com.nice.stock.entities.LigneCommandeClient;
import com.nice.stock.services.ILigneCommandeClientService;

@Transactional
public class LigneCommandeClientServiceImpl implements ILigneCommandeClientService {

	private ILigneCommandeClientDao ligneCommandeClientDao;
	public void setLigneCommandeClientDao(ILigneCommandeClientDao ligneCommandeClientDao) {
		this.ligneCommandeClientDao = ligneCommandeClientDao;
	}

	@Override
	public LigneCommandeClient save(LigneCommandeClient entity) {
		// TODO Auto-generated method stub
		return ligneCommandeClientDao.save(entity);
	}

	@Override
	public LigneCommandeClient update(LigneCommandeClient entity) {
		// TODO Auto-generated method stub
		return ligneCommandeClientDao.update(entity);
	}

	@Override
	public List<LigneCommandeClient> selectAll() {
		// TODO Auto-generated method stub
		return ligneCommandeClientDao.selectAll();
	}

	@Override
	public List<LigneCommandeClient> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return ligneCommandeClientDao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeClient getByIg(Long id) {
		// TODO Auto-generated method stub
		return ligneCommandeClientDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		ligneCommandeClientDao.remove(id);
		
	}

	@Override
	public LigneCommandeClient findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return ligneCommandeClientDao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeClient findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return ligneCommandeClientDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return ligneCommandeClientDao.findCountBy(paramName, paramValue);
	}

}
