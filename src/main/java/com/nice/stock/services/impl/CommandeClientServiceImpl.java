package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.ICommandeClientDao;
import com.nice.stock.entities.CommandeClient;
import com.nice.stock.services.ICommandeClientService;

@Transactional
public class CommandeClientServiceImpl implements ICommandeClientService {

	private ICommandeClientDao commandeClientDao;
	
	public void setCommandeClientDao(ICommandeClientDao commandeClientDao) {
		this.commandeClientDao = commandeClientDao;
	}

	@Override
	public CommandeClient save(CommandeClient entity) {
		// TODO Auto-generated method stub
		return commandeClientDao.save(entity);
	}

	@Override
	public CommandeClient update(CommandeClient entity) {
		// TODO Auto-generated method stub
		return commandeClientDao.update(entity);
	}

	@Override
	public List<CommandeClient> selectAll() {
		// TODO Auto-generated method stub
		return commandeClientDao.selectAll();
	}

	@Override
	public List<CommandeClient> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return commandeClientDao.selectAll(sortField, sort);
	}

	@Override
	public CommandeClient getByIg(Long id) {
		// TODO Auto-generated method stub
		return commandeClientDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
	commandeClientDao.remove(id);
		
	}

	@Override
	public CommandeClient findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return commandeClientDao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeClient findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return commandeClientDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return commandeClientDao.findCountBy(paramName, paramValue);
	}

}
