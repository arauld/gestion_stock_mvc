package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.ILigneCommandeFournisseurDao;
import com.nice.stock.entities.LigneCommandeFournisseur;
import com.nice.stock.services.ILigneCommandeFournisseurService;

@Transactional
public class LigneCommandeFournisseurServiceImpl implements ILigneCommandeFournisseurService {

	private ILigneCommandeFournisseurDao ligneCommandeFournisseurDao;
	
	public void setLigneCommandeFournisseurDao(ILigneCommandeFournisseurDao ligneCommandeFournisseurDao) {
		this.ligneCommandeFournisseurDao = ligneCommandeFournisseurDao;
	}

	@Override
	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return ligneCommandeFournisseurDao.save(entity);
	}

	@Override
	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return ligneCommandeFournisseurDao.update(entity);
	}

	@Override
	public List<LigneCommandeFournisseur> selectAll() {
		// TODO Auto-generated method stub
		return ligneCommandeFournisseurDao.selectAll();
	}

	@Override
	public List<LigneCommandeFournisseur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return ligneCommandeFournisseurDao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeFournisseur getByIg(Long id) {
		// TODO Auto-generated method stub
		return ligneCommandeFournisseurDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		ligneCommandeFournisseurDao.remove(id);
	}

	@Override
	public LigneCommandeFournisseur findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return ligneCommandeFournisseurDao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeFournisseur findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return ligneCommandeFournisseurDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return ligneCommandeFournisseurDao.findCountBy(paramName, paramValue);
	}

}
