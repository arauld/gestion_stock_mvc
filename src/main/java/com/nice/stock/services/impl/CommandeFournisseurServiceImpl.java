package com.nice.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.nice.stock.dao.ICommandeFournisseurDao;
import com.nice.stock.entities.CommandeFournisseur;
import com.nice.stock.services.ICommandeFournisseurService;
@Transactional
public class CommandeFournisseurServiceImpl implements ICommandeFournisseurService {

	private ICommandeFournisseurDao commandeFournisseurDao;
	
	public void setCommandeFournisseurDao(ICommandeFournisseurDao commandeFournisseurDao) {
		this.commandeFournisseurDao = commandeFournisseurDao;
	}

	@Override
	public CommandeFournisseur save(CommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return commandeFournisseurDao.save(entity);
	}

	@Override
	public CommandeFournisseur update(CommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return commandeFournisseurDao.update(entity);
	}

	@Override
	public List<CommandeFournisseur> selectAll() {
		// TODO Auto-generated method stub
		return commandeFournisseurDao.selectAll();
	}

	@Override
	public List<CommandeFournisseur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return commandeFournisseurDao.selectAll(sortField, sort);
	}

	@Override
	public CommandeFournisseur getByIg(Long id) {
		// TODO Auto-generated method stub
		return commandeFournisseurDao.getByIg(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		commandeFournisseurDao.remove(id);
		
	}

	@Override
	public CommandeFournisseur findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return commandeFournisseurDao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeFournisseur findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return commandeFournisseurDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return commandeFournisseurDao.findCountBy(paramName, paramValue);
	}

}
