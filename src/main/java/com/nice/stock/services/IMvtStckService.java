package com.nice.stock.services;

import java.util.List;

import com.nice.stock.entities.MvtStock;



public interface IMvtStckService {

	public MvtStock save(MvtStock entity);
	public MvtStock update(MvtStock entity);
	public List<MvtStock> selectAll();
	
	public List<MvtStock> selectAll(String sortField, String sort);
	public MvtStock getByIg(Long id);
	public void remove(Long id);
	
	public MvtStock findOne(String paramName,Object paramValue);
	public MvtStock findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
}
