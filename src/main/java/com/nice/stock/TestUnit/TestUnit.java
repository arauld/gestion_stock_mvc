package com.nice.stock.TestUnit;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.nice.stock.dao.IClientDao;
import com.nice.stock.entities.Categorie;
import com.nice.stock.entities.Client;
import com.nice.stock.entities.Fournisseur;
import com.nice.stock.services.ICategorieService;
import com.nice.stock.services.IClientService;
import com.nice.stock.services.IFournisseurService;

public class TestUnit {
	
	ClassPathXmlApplicationContext context;

	@Before
	public void setUp() throws Exception {
		//Chargement du contexte de l'application
		context = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml" });
	}

	/*@Test
	public void test() {
		try {
			IClientService clientService = (IClientService) context.getBean("clientService");
			Client c = new Client();
			c.setAdresse("Angré caféier 4");
			c.setEmail("acka@gmail.com");
			c.setNom("Ackah");
			c.setPrenom("Andoh");
			c.setPhoto("https:flickr.com");
		clientService.save(c);
			
		}
	    catch (Exception e) {
		assertTrue(e.getMessage(), false);
	}
			
	}*/
	
  /*@Test
  public void test1(){
	  
	  try{
	 IFournisseurService fournisseurService = (IFournisseurService) context.getBean("FournisseurService");
	  Fournisseur f = new Fournisseur();
	  f.setNom("Koffi");
	  f.setPrenom("norbert");
	  f.setEmail("norbert@gmail.com");
	  f.setAdresse("marcory INJS");
	  f.setPhoto("https:flickr.com");
	   fournisseurService.save(f);
	   
	  } catch(Exception e){
		  
		  assertTrue(e.getMessage(), false);
	  }
  } */
	
	@Test
	public void test(){
		try{
			ICategorieService categorieServcie = (ICategorieService) context.getBean("cateogorieService");
			Categorie c = new Categorie();
			c.setCode("BIO");
			c.setDesignation("produit bio éprouvés");
			categorieServcie.save(c);
			
		} catch(Exception e){
			
		}
	}
}
